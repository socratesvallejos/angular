import {Component, OnChanges, OnInit} from '@angular/core';
import { User } from '../user';
import { FormGroup, FormControl, Validators } from '@angular/forms'


@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {
  userList: User[]=[];

  form: FormGroup;

  addUser(form){
     //console.log(form.value);
    this.userList.push(this.form.value);
  }

  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('',[Validators.required, Validators.pattern('[a-zA-Z][a-zA-z ]+')]),
      description: new FormControl('',[Validators.required, Validators.pattern('[a-zA-Z][a-zA-z ]+')]),
      device_resource_type: new FormControl('',[Validators.required, Validators.pattern('[a-zA-Z][a-zA-z ]+')]),
      default_value: new FormControl('',[Validators.required, Validators.pattern('[a-zA-Z][a-zA-z ]+')]),
      data_type: new FormControl('',[Validators.required, Validators.pattern('[a-zA-Z][a-zA-z ]+')]),
      format: new FormControl('',[Validators.required, Validators.pattern('[a-zA-Z][a-zA-z ]+')]),
      range_min: new FormControl('',[Validators.required, Validators.pattern('[0-9]*')]),
      range_max: new FormControl('',[Validators.required, Validators.pattern('[0-9]*')]),
      unit_of_measurement: new FormControl('',[Validators.required, Validators.pattern('[0-9]*')]),
      precision: new FormControl('',[Validators.required, Validators.pattern('[0-9]*')]),
      accuracy: new FormControl('',[Validators.required, Validators.pattern('[0-9]*')]),

    })
  }

}
