export interface User{
    name : string,
    description: string,
    device_resource_type: string
    default_value: string
    data_type: string
    format: string
    range_min: number
    range_max: number
    unit_of_measurement: number
    precision: number
    accuracy: number
}
